CH32V003F4P6-R0-1v1
===================

This is an Altium-to-KiCad conversion of the WCH CH32V003F4P6-R0-1v1 eval
board.  After the conversion, the symbols and footprints imported from the
original Altium files were replaced with their KiCad equivalents, and the
symbol for the CH32V003 itself (available separately from [my KiCad library
collection](https://gitlab.com/salfter/kicad-libs)) was edited into
something closer to following KiCad convention.

This should be nearly a hole-for-hole, trace-for-trace conversion.  The only
somewhat substantial change was that the mounting holes for the power switch
are 8.2 mm apart instead of 9.0 mm apart.  8.2 mm appears to be the standard
spacing for small SPDT switches available from Western distributors.  A
small handful of the vias that join the fills may have been moved or
removed.

The original files were downloaded from [the OpenWCH GitHub
repo](https://github.com/openwch/ch32v003/tree/main/SCHPCB/CH32V003F4P6-R0-1v1/SCH_PCB). 
I converted them with
[altium2kicad](https://github.com/thesourcerer8/altium2kicad/).  Lots of
editing then followed to get the schematic and PCB into an editable and
maintainable form.  Gerbers and a STEP file for the board are included.
